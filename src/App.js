import React, {Component} from 'react';
import './App.css';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import MainWrapper from './components/mainwrapper/MainWrapper';
import {CITRINE, RICH_BLACK, RUBY_RED, SPARTAN_CRIMSON} from "./constants";


const getTheme = () => {
    let overwrites = {
        palette: {
            primary: {
                main: RICH_BLACK
            },
            secondary: {
                main: SPARTAN_CRIMSON
            },
            error: {
                main: RUBY_RED
            },
            type: 'dark',
        },
        typography:  {
            headline: {
                color: CITRINE,
                marginBottom: 40
            },
            display3: {
                color: CITRINE
            },
            display2: {
                color: CITRINE
            },
            display1: {
                color: CITRINE
            },
            subheading: {
                margin: 20
            }
        }
    };
    return createMuiTheme(overwrites);
};

class App extends Component {


    render() {
        return (
            <MuiThemeProvider theme={getTheme()}>
                <MainWrapper/>
            </MuiThemeProvider>
        );
    }
}

export default App;
