import React from 'react';
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField';
import DialogContent from '@material-ui/core/DialogContent';
import './Dialogs.css';
import { withContext } from "../../context";


const LoginDialogContent = props => {

    return (
            <DialogContent>
                <div>
                    <TextField
                        label="Email Address"
                        type="email"
                        value={props.inputEmail}
                        onChange={props.onInputEmailChange}
                    />
                </div>
                <div>
                    <TextField
                        label="Password"
                        type="password"
                        value={props.inputPassword}
                        onChange={props.onInputPasswordChange}
                    />
                </div>
            </DialogContent>
    )
};

LoginDialogContent.propTypes = {
    inputEmail: PropTypes.string.isRequired,
    inputPassword: PropTypes.string.isRequired,
    onInputEmailChange: PropTypes.func.isRequired,
    onInputPasswordChange: PropTypes.func.isRequired,
};

export default withContext(LoginDialogContent);

