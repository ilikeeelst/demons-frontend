import React, {Fragment} from "react";
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography'
import CircularProgress from '@material-ui/core/CircularProgress';
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";
import "./ImageCarousel.css";
import {withContext} from "../../context";


const ImageCarousel = props => {
    let carousel;
    let dialogText;
    if (props.open ) {

        if (props.loading) {
            dialogText = "Loading Images...";
            carousel =
                <CircularProgress/>
        }
        else {
            const images = props.images.map((x) => {
                return ({
                    original: x.url,
                    originalTitle: x.name,
                    originalAlt: x.name
                });
            });
            carousel =
                <ImageGallery
                    items={images}
                    showPlayButton={false}
                    showFullscreenButton={false}
                    onSlide={props.onSlideChange}
                    showThumbnails={false}
                    showBullets={true}
                    additionalClass="carousel"
                />
        }
    }
    else {
        carousel = null;
    }
    return (
        <Fragment>
            <div>
                <Typography color="textPrimary" variant="subheading">
                    {dialogText}
                </Typography>
            </div>
            <div className="carousel-wrapper">
                {carousel}
            </div>
        </Fragment>
    )
};

ImageCarousel.propTypes = {
    images: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    onSlideChange: PropTypes.func.isRequired,
    open: PropTypes.bool
};

export default withContext(ImageCarousel);