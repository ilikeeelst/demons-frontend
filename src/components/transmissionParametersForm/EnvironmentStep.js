import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControl from '@material-ui/core/FormControl';

const EnvironmentStep = props => {
    return (
        <React.Fragment>
            <Typography variant="title" gutterBottom>
                You can run the transmission through a simulated channel or over-the-air through the testbed
                (when available)
            </Typography>
                <FormControl>
                    <RadioGroup
                        aria-label="Environment"
                        name="Environment"
                        value={props.environment}
                        onChange={props.onEnvironmentChange}
                        className="alignRight"
                    >
                        <FormControlLabel
                            value="simulation"
                            control={<Radio />}
                            label="Simulation"/>
                        <FormControlLabel
                            value="testbed"
                            disabled={!props.testBedIsAvailable}
                            control={<Radio />}
                            label="Testbed"/>
                    </RadioGroup>
                </FormControl>
            {props.children}
        </React.Fragment>
    );
};

EnvironmentStep.propTypes = {
    testBedIsAvailable: PropTypes.bool.isRequired,
    environment: PropTypes.string.isRequired,
    onEnvironmentChange: PropTypes.func.isRequired,
    children: PropTypes.node
};

export default EnvironmentStep;
