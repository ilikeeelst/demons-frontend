import React from 'react';
import { Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import TransmissionsTable from './TransmissionsTable';
import {getTransmissionResultsRoute, getTransmissionRoute} from '../../routes'
import getTransmissionsListAction from "../../actions/getTransmissionsListAction";
import fetchImageDetails from '../../actions/fetchImageDetails';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing.unit * 7,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing.unit * 9,
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
        height: '100vh',
        overflow: 'auto',
    },
    chartContainer: {
        marginLeft: -22,
    },
    tableContainer: {
        height: 320,
    },
});

class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            transmissions: [],
            selectedTransmissionIndex: 0,
            imageDialogIsOpen: false,
            imageSrc: null
        };
    }


    componentDidMount() {
        window.scrollTo(0,0);
        getTransmissionsListAction()
        .then( data => {
            this.setState({
                    transmissions: data.transmissions,
                    isLoadingImages: false
                }
            )
        });

    }

    openImageDialog = (imageId) => {
        fetchImageDetails(imageId)
            .then(data => {
               this.setState({
                   imageSrc: data.url,
                   imageDialogIsOpen: true
               })
            });
    };


    closeImageDialog = () => {
        this.setState({
            imageDialogIsOpen: false,
        });
    };
    render() {
        const { classes } = this.props;

        return (
            <React.Fragment>
                <Switch>
                    {getTransmissionResultsRoute(true)}
                    {getTransmissionRoute(true)}
                </Switch>
                <CssBaseline />
                <div className={classes.root}>
                    <main className={classes.content}>
                        <div className={classes.appBarSpacer} />
                        <Typography variant="display1" gutterBottom>
                            My transmissions
                        </Typography>
                        <div className={classes.tableContainer}>
                            <TransmissionsTable
                                transmissions={this.state.transmissions}
                                onCloseImageDialog={this.closeImageDialog}
                                onViewImageClick={this.openImageDialog}
                                imageDialogIsOpen={this.state.imageDialogIsOpen}
                                inputImageSrc={this.state.imageSrc}
                            />
                        </div>
                    </main>
                </div>
            </React.Fragment>
        );
    }
}

HomePage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HomePage);