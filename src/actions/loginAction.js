import {
    API_BASE_URL,
    LOGIN_ENDPOINT, LOGIN_REQUEST_CREDENTIALS,
    LOGIN_REQUEST_HEADERS,
    LOGIN_REQUEST_METHOD,
    LOGIN_REQUEST_MODE
} from "../constants";
import {fetchUrl} from "./utils";


export default (email, password) => {
    console.log("Sending login request...");
    const fetchOptions = {
        method: LOGIN_REQUEST_METHOD,
        headers: LOGIN_REQUEST_HEADERS,
        body: JSON.stringify({
            "email": email,
            "password": password
        }),
        mode: LOGIN_REQUEST_MODE,
        credentials: LOGIN_REQUEST_CREDENTIALS
    };
    return fetchUrl(API_BASE_URL + LOGIN_ENDPOINT, fetchOptions)
        .then( response => {
            if (response.status !== 200) {
                console.warn("The server refuses to log the user in. STATUS: " + response.status);
            }
            return response.body;
        })
        .catch( err => {
            console.log(("Error occurred while trying to log in"));
            console.error(err);
            return {
                authenticated: false
            }
        })
};
