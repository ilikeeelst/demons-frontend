import React from "react";
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import CircularProgress from '@material-ui/core/CircularProgress';
import LoginDialogContent from './LoginDialogContent';
import SignupDialogContent from './SignupDialogContent';
import './Dialogs.css';
import LogoutDialogContent from "./LogoutDialogContent";

const  AuthenticationDialog = props => {
    let dialogContent;
    if (props.isWaitingForServerResponse) {
        dialogContent =  <CircularProgress className="dialogProgress"/>
    }
    else {
        if (props.variant === 'login') {
            dialogContent = <LoginDialogContent/>;
        }
        if (props.variant === 'signup') {
            dialogContent = <SignupDialogContent/>

        }
        if (props.variant === 'logout') {
            dialogContent = <LogoutDialogContent/>
        }
    }
    return (
        <Dialog open={props.open} onClose={props.closeDialog}>
            <DialogTitle>
                {props.dialogTitle}
            </DialogTitle>
            {dialogContent}
            <DialogActions>
                <Button onClick={props.closeDialog} color="primary">
                    Cancel
                </Button>
                <Button onClick={props.handleConfirm} color="primary" disabled={!props.dataAreValid()}>
                    Confirm
                </Button>
            </DialogActions>
        </Dialog>
    )
};

AuthenticationDialog.propTypes = {
    open: PropTypes.bool,
    closeDialog: PropTypes.func,
    isWaitingForServerResponse: PropTypes.bool,
    dialogTitle : PropTypes.string.isRequired,
    handleConfirm: PropTypes.func,
    dataAreValid: PropTypes.func,
    variant: PropTypes.oneOf(['login', 'signup', 'logout'])
};

export default AuthenticationDialog;