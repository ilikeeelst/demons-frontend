import React, {Component} from 'react';
import { Switch } from 'react-router-dom';
import Appbar from '../appbar/Appbar';
import SideMenu from '../menu/SideMenu'
import TypedSnackbar from '../snackbars/TypedSnackbar';
import Footer from '../footer/Footer';
import AuthenticationDialog from '../dialogs/AuthenticationDialog';
import signUpAction from "../../actions/signUpAction";
import loginAction from '../../actions/loginAction';
import { Provider } from '../../context'
import {
    getHomeRoute,
    getTutorialRoute,
    getAdminRoute,
} from "../../routes";
import {STORAGE_USERNAME_FIELD, TOKEN_FIELD_NAME} from "../../constants";
import { stringIsEmail } from '../../utils';


class MainWrapper extends Component {
    constructor (props) {
        super(props);
        let username = "";
        let loggedIn = false;
        let accessToken = localStorage.getItem(TOKEN_FIELD_NAME);
        if (accessToken) {
            username = localStorage.getItem(STORAGE_USERNAME_FIELD);
            loggedIn = true
        }
        this.state = {
            snackbarIsOpen: false,
            dialogIsOpen: false,
            username: username,
            password: null,
            inputEmail: "",
            inputPassword: "",
            inputConfirmationPassword: "",
            isLoggedIn: loggedIn,
            isWaitingForServerResponse: false,
            menuIsOpen: false
        };
        this.dialogProps = {
            variant: "login",
            dialogTitle: "",
            open: false,
            closeDialog: this.closeLoginDialog.bind(this),
            handleConfirm: this.handleLogin.bind(this),
            dataAreValid: () => false
        };

        this.snackbarProps = {
            onClose: this.closeSnackBar.bind(this),
            variant: "info",
            message: "info",
            autoHideDuration: 3000
        };
    }

    getContext = () => ({
        inputEmailIsValid: this.inputEmailIsValid.bind(this),
        inputPasswordIsValid: this.inputPasswordIsValid.bind(this),
        inputConfirmationPasswordIsValid: this.inputConfirmationPasswordIsValid.bind(this),
        onInputEmailChange: this.onInputEmailChange.bind(this),
        onInputPasswordChange: this.onInputPasswordChange.bind(this),
        onInputConfirmationPasswordChange: this.onInputConfirmationPasswordChange.bind(this),
        openLoginDialog: this.openLoginDialog.bind(this),
        closeLoginDialog: this.closeLoginDialog.bind(this),
        openSignupDialog: this.openSignupDialog.bind(this),
        closeSignupDialog: this.closeSignupDialog.bind(this),
        openLogoutDialog: this.openLogoutDialog.bind(this),
        closeLogoutDialog: this.closeLogoutDialog.bind(this),
        handleSignup: this.handleSignup.bind(this),
        handleLogin: this.handleLogin.bind(this),
        handleLogout: this.handleLogout.bind(this),
        ...this.state
    });

    inputEmailIsValid () {
        return stringIsEmail(this.state.inputEmail)
    }

    inputPasswordIsValid () {
        return this.state.inputPassword.length > 0;
    }

    inputConfirmationPasswordIsValid () {
        return this.state.inputConfirmationPassword === this.state.inputPassword
    }

    loginDataAreValid() {
        return (
            this.state.inputEmail.length > 0
            && this.state.inputPassword.length >0)
    }

    signupDataAreValid() {
        return (
            this.inputEmailIsValid()
            && this.inputPasswordIsValid()
            && this.inputConfirmationPasswordIsValid()
        )
    }

    closeSnackBar() {
        this.setState({
            snackbarIsOpen: false
        })
    }


    closeLoginDialog() {
        this.dialogProps.open = false;
        this.setState({
                dialogIsOpen: false,
                inputEmail: "",
                inputPassword: "",
                inputConfirmationPassword: ""
            }
        );
    }

    openLoginDialog() {
        this.dialogProps = Object.assign(this.dialogProps,{
            variant: "login",
            dialogTitle: "Log in",
            open: true,
            closeDialog: this.closeLoginDialog.bind(this),
            handleConfirm: this.handleLogin.bind(this),
            dataAreValid: this.loginDataAreValid.bind(this)
        });
        this.setState({
                dialogIsOpen: true
            }
        );
    }

    closeSignupDialog() {
        this.dialogProps.open=false;
        this.setState({
                dialogIsOpen: false,
                inputEmail: "",
                inputPassword: "",
                inputConfirmationPassword: ""
            }
        );
    }

    openSignupDialog() {
        this.dialogProps = Object.assign(this.dialogProps,{
            variant: "signup",
            dialogTitle: "Sign up",
            open: true,
            closeDialog: this.closeSignupDialog.bind(this),
            handleConfirm: this.handleSignup.bind(this),
            dataAreValid: this.signupDataAreValid.bind(this)
        });
        this.setState({
                dialogIsOpen: true
            }
        );
    }

    closeLogoutDialog() {
        this.dialogProps.open=false;
        this.setState({
                logoutDialogIsOpen: false
            }
        );
    }

    openLogoutDialog() {
        this.dialogProps = Object.assign(this.dialogProps,{
            variant: "logout",
            dialogTitle: "Are you sure?",
            open: true,
            closeDialog: this.closeLogoutDialog.bind(this),
            handleConfirm: this.handleLogout.bind(this),
            dataAreValid: () => true
        });
        this.setState({
                logoutDialogIsOpen: true
            }
        );
    }


    onInputEmailChange(event) {
        this.setState({
            inputEmail: event.target.value
        });
    }

    onInputPasswordChange(event) {
        this.setState({
            inputPassword: event.target.value
        });
    }

    onInputConfirmationPasswordChange(event) {
        this.setState({
            inputConfirmationPassword: event.target.value
        });
    }

    handleSignup = () => {
        this.setState({
            isWaitingForServerResponse: true
        });
        signUpAction(this.state.inputEmail, this.state.inputPassword)
            .then(signupSuccessful => {
                    let message = this.state.snackbarMessage;
                    let variant = this.state.snackbarVariant;
                    if (signupSuccessful) {
                        message = "Account created successfully!";
                        variant = "success";
                    }
                    else {
                        message = "An error occurred while creating the account";
                        variant = "error";
                    }
                    this.dialogProps.open=false;
                this.snackbarProps.message=message;
                this.snackbarProps.variant = variant;
                    this.setState({
                        snackbarIsOpen: true,
                        isWaitingForServerResponse: false,
                        dialogIsOpen: false,
                        inputEmail: "",
                        inputPassword: "",
                        inputConfirmationPassword: ""
                    });

                }
            );

    };

    handleLogin = () => {
        this.setState({
            isWaitingForServerResponse: true
        });
        loginAction(this.state.inputEmail, this.state.inputPassword)
            .then(data => {
                    let message = this.state.snackbarMessage;
                    let variant = this.state.snackbarVariant;
                    if (data.authenticated) {
                        message = "Logged in successfully!";
                        variant = "success";
                        localStorage.setItem(STORAGE_USERNAME_FIELD, data["email"]);
                        localStorage.setItem(TOKEN_FIELD_NAME, data[TOKEN_FIELD_NAME]);
                        this.setState({
                            isLoggedIn: true,
                            username: data.email
                        });
                    }
                    else {
                        message = "An error occurred while trying to log in";
                        variant = "error";
                    }
                    this.dialogProps.open=false;
                    this.snackbarProps.message=message;
                    this.snackbarProps.variant = variant;
                    this.setState( {
                        snackbarIsOpen: true,
                        isWaitingForServerResponse: false,
                        dialogIsOpen: false,
                        inputEmail: "",
                        inputPassword: "",
                        inputConfirmationPassword: ""
                    })
                    ;
                }
            );
    };

    handleLogout () {
        localStorage.removeItem(TOKEN_FIELD_NAME);
        localStorage.removeItem(STORAGE_USERNAME_FIELD);
        this.setState({
            username: "",
            userToken: "",
            isLoggedIn: false
        });
        this.dialogProps.open=false;
    }

    toggleMenuState () {
        this.setState({
            menuIsOpen: !this.state.menuIsOpen
        })
    }

    closeMenu () {
        this.setState({
            menuIsOpen: false
        })
    }

    openMenu () {
        this.setState({
            menuIsOpen: true
        })
    }

    render() {
        return (
            <Provider value={this.getContext()}>
                <div className="App">
                    <Appbar
                        isLoggedIn={this.state.isLoggedIn}
                        username={this.state.inputEmail}
                        openLoginDialog={this.openLoginDialog.bind(this)}
                        openSignupDialog={this.openSignupDialog.bind(this)}
                        closeLoginDialog={this.closeLoginDialog.bind(this)}
                        closeSignupDialog={this.closeSignupDialog.bind(this)}
                        openLogoutDialog={this.openLogoutDialog.bind(this)}
                        onMenuButtonClick={this.toggleMenuState.bind(this)}
                    />
                    <AuthenticationDialog
                        isWaitingForServerResponse={this.state.isWaitingForServerResponse}
                        {...this.dialogProps}
                    />
                    <SideMenu
                        mobileOpen={this.state.menuIsOpen}
                        onClose={this.closeMenu.bind(this)}
                        onOpen={this.openMenu.bind(this)}
                    >
                    </SideMenu>
                    <Switch>
                        {getAdminRoute(true)}
                        {getHomeRoute({
                            isLoggedIn: this.state.isLoggedIn
                        })}
                        {getTutorialRoute()}
                    </Switch>
                    <TypedSnackbar
                        open={ this.state.snackbarIsOpen}
                        {...this.snackbarProps}
                    />
                    <Footer/>
                </div>
            </Provider>
        );
    }
}

export default MainWrapper;
