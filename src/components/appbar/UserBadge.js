import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import pink from '@material-ui/core/colors/pink';
import green from '@material-ui/core/colors/green';
import Avatar from '@material-ui/core/Avatar';
import Face from '@material-ui/icons/Face';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { withContext } from '../../context';

const styles = {
    avatar: {
        margin: 10,
    },
    pinkAvatar: {
        margin: 10,
        color: '#fff',
        backgroundColor: pink[500],
    },
    greenAvatar: {
        margin: 10,
        color: '#fff',
        backgroundColor: green[500],
    },
    row: {
        display: 'flex',
        justifyContent: 'center',
    },
};

const UserBadge = props => {
    const { classes } = props;
    return (
            <Fragment >
                <Typography variant="body1" color="inherit">
                    {props.username}
                </Typography>
                <Avatar className={classes.avatar}>
                    <Face />
                </Avatar>
                <IconButton color="inherit">
                    <Badge className={classes.margin} badgeContent={1} color="secondary">
                        <NotificationsIcon />
                    </Badge>
                </IconButton>
            </Fragment>
    );
};

UserBadge.propTypes = {
    classes: PropTypes.object.isRequired,
    username: PropTypes.string.isRequired
};

export default withContext(withStyles(styles) (UserBadge));
