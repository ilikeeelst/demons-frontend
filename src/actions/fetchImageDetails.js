import {fetchUrl} from "./utils";
import {
    API_BASE_URL, GET_IMAGE_DETAIL_METHOD, GET_IMAGE_DETAIL_MODE, IMAGE_DETAILS_ENDPOINT
} from "../constants";


export default (imageId) => {
    console.log("fetching image data...");
    const fetchOptions = {
        method: GET_IMAGE_DETAIL_METHOD,
        mode: GET_IMAGE_DETAIL_MODE
    };
    return fetchUrl(API_BASE_URL + IMAGE_DETAILS_ENDPOINT + "/" + imageId, fetchOptions)
        .then(response => {
            if (response.status === 200) {
                return response.body;
            }
            else {
                throw new Error("Something went wrong while fetching the image data");
            }
        })
}