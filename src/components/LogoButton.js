import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';



const LogoButton = () => {
    return(
        <div>
            <Link to="/" className="logoButton"/>
        </div>

    )
};

LogoButton.propTypes = {
  className: PropTypes.string
};



export default LogoButton;