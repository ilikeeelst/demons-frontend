import {
    API_BASE_URL, TRANSMISSION_ENDPOINT, TRANSMISSIONS_LIST_REQUEST_CREDENTIALS,
    TRANSMISSIONS_LIST_REQUEST_METHOD, TRANSMISSIONS_LIST_REQUEST_MODE,
} from "../constants";
import {fetchUrl} from "./utils";


export default (transmissionId) => {
    console.log("Fetching transmissions list...");
    const fetchOptions = {
        method: TRANSMISSIONS_LIST_REQUEST_METHOD,
        mode: TRANSMISSIONS_LIST_REQUEST_MODE,
        credentials: TRANSMISSIONS_LIST_REQUEST_CREDENTIALS
    };
    return fetchUrl(API_BASE_URL + TRANSMISSION_ENDPOINT + "/" + transmissionId, fetchOptions)
        .then( response => {
            if (response.status !== 200) {
                console.warn("The server reported an error while fetching the user's transmissions list. STATUS: "
                    + response.status);
            }
            return response.body
        })
        .catch( err => {
            console.log(("Error occurred while fetching the user's transmissions list:"));
            console.error(err);
            return {
                success: false
            }
        })
};
