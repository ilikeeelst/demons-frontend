import {
    API_BASE_URL,
    TRANSMISSION_CREDENTIALS,
    TRANSMISSION_METHOD,
    TRANSMISSION_MODE,
    TRANSMISSIONS_LIST_ENDPOINT
} from "../constants";
import {fetchUrl} from "./utils";


export default () => {
    console.log("Fetching transmission data...");
    const fetchOptions = {
        method: TRANSMISSION_METHOD,
        mode: TRANSMISSION_MODE,
        credentials: TRANSMISSION_CREDENTIALS
    };
    return fetchUrl(API_BASE_URL + TRANSMISSIONS_LIST_ENDPOINT, fetchOptions)
        .then( response => {
            if (response.status !== 200) {
                console.warn("The server reported an error while fetching the transmission data"
                    + response.status);
               //window.localStorage.removeItem(TOKEN_FIELD_NAME)
            }
            else {
                return response.body;
            }
        })
        .catch( err => {
            console.log(("Error occurred while fetching the transmission data:"));
            console.error(err);
            return {
                success: false,
                transmissions: []
            }
        })
};
