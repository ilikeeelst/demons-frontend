import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import {withContext} from "../../context";
import {enviromentParameters, parametersDescription} from "../../constants";


const styles = theme => ({
    listItem: {
        padding: `${theme.spacing.unit}px 0`,
    },
    title: {
        marginTop: theme.spacing.unit * 2,
    },
});

function Review(props) {
    const {classes} = props;
    const generalParameters = [
        {name: 'Environment', value: props.environment}
    ];

    let image;

    if (props.capturedImage != null && props.webcamIsOpen) {
        image = props.capturedImage
    }
    else {
        image = props.images[props.selectedImageIndex].url
    }
    return (
        <React.Fragment>
            <Typography variant="title" gutterBottom>
                Transmission Summary
            </Typography>
            <Grid container spacing={16}>
                <Grid item xs={12}>
                    <Typography variant="subheading" gutterBottom>
                        Image chosen for the transmission:
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <img src={image}>
                    </img>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="subheading" gutterBottom>
                        Parameters of the transmission:
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <List disablePadding>
                        {generalParameters.map(parameter => (
                            <ListItem className={classes.listItem} key={parameter.name}>
                                <ListItemText
                                    primary={parameter.name}
                                    secondary={parametersDescription[parameter.value]}
                                />
                                <Typography variant="body2">{parameter.value}</Typography>
                            </ListItem>
                        ))}
                        {enviromentParameters[props.environment].map(parameter => (
                            <ListItem className={classes.listItem} key={parameter}>
                                <ListItemText primary={parameter} secondary={parametersDescription[parameter]}/>
                                <Typography variant="body2">{props[parameter]}</Typography>
                            </ListItem>
                        ))}
                    </List>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}

Review.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withContext(withStyles(styles)(Review));
