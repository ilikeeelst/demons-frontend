import {TOKEN_FIELD_NAME, TOKEN_HEADER_NAME, TOKEN_PREFIX} from "../constants";

export const fetchUrl = (url, options) => {
    const accessToken = localStorage.getItem(TOKEN_FIELD_NAME);
    if(accessToken) {
        if (!options.headers) {
            options.headers = {}
        }
        options.headers[TOKEN_HEADER_NAME] = TOKEN_PREFIX +
            ' '
            + accessToken
    }

    return fetch(url, options)
        .then(response =>
            response.json().then(json => {
                return {
                    status: response.status,
                    headers: response.headers,
                    body: json
                };
            })
        );
};