import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {Link} from "react-router-dom";
import {getDatetimeString} from "../../utils";

const styles = {
    root: {
        width: '100%',
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
};


const TransmissionsTable  = props => {

        const {transmissions, classes} = props;
        return (
            <Paper className={classes.root}>
                <Dialog open={props.imageDialogIsOpen} onClose={props.onCloseImageDialog}>
                    <DialogContent>
                        <img src={props.inputImageSrc}>
                        </img>
                    </DialogContent>
                </Dialog>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Date/Time</TableCell>
                            <TableCell>Environment</TableCell>
                            <TableCell>Input image</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {transmissions.map(transmission => {
                            return (
                                <TableRow key={transmission.id}>
                                    <TableCell component="th" scope="row">
                                        {getDatetimeString(transmission.datetime)}
                                    </TableCell>
                                    <TableCell>{transmission.environment}</TableCell>
                                    <TableCell>
                                        <Button
                                            variant="outlined"
                                            onClick={() => {
                                                props.onViewImageClick(transmission.imageId)
                                            }}

                                        >
                                            View
                                        </Button>
                                    </TableCell>
                                    <TableCell>{transmission.results}</TableCell>
                                    <TableCell>
                                        <Button
                                            variant="contained"
                                            color="secondary"
                                            component={
                                                props => <Link to={{
                                                    pathname: "/results/" + transmission.id,

                                                }} {...props}/>
                                            }
                                        >
                                            REVIEW
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        );

};


TransmissionsTable.propTypes = {
    classes: PropTypes.object.isRequired,
    transmissions: PropTypes.array.isRequired,
    onViewImageClick: PropTypes.func.isRequired,
    onCloseImageDialog: PropTypes.func.isRequired,
    imageDialogIsOpen: PropTypes.bool.isRequired,
    inputImageSrc: PropTypes.string.isRequired
};

export default withStyles(styles)(TransmissionsTable);
