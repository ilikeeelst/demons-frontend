import React from 'react'
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { withContext } from "../../context";
import Grid from "@material-ui/core/Grid/Grid";
import {channelTypes, simulationTransmissionTypes, SNR_MAX_VAL, SNR_MIN_VAL, SNR_STEP} from '../../constants';

const SimulationParameters = props => {
    let channelTypeOptions = channelTypes.map(x => (
        <MenuItem key={x} value={x}>
            {x}
        </MenuItem>
    ));
    let transmissionTypeOptions = simulationTransmissionTypes.map(x => (
        <MenuItem key={x} value={x}>
            {x}
        </MenuItem>
    ));
    return (
        <Grid container spacing={24}>
            <Grid item xs={12}>
                <FormControl fullWidth>
                    <InputLabel>
                        SNR
                    </InputLabel>
                    <Input
                        error={!props.snrIsValid()}
                        id="snr"
                        value={props.snr}
                        onChange={(event) => {
                            props.onFieldChange("snr", event)
                        }}
                        endAdornment={<InputAdornment position="end">dB</InputAdornment>}
                        type="number"
                        inputProps={{
                            step: SNR_STEP,
                            min: SNR_MIN_VAL,
                            max: SNR_MAX_VAL
                        }}
                    />
                </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
                <TextField
                    select
                    label="channel"
                    value={props.channelType}
                    onChange={(event) => {
                        props.onFieldChange("channelType", event)
                    }}
                    margin="normal"
                    fullWidth
                >
                    {channelTypeOptions}
                </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
                <TextField
                    select
                    label="scenario"
                    value={props.transmissionType}
                    onChange={event => {
                        props.onFieldChange("transmissionType", event)
                    }}
                    margin="normal"
                    fullWidth
                >
                    {transmissionTypeOptions}
                </TextField>
            </Grid>
        </Grid>
    )
};

SimulationParameters.propTypes = {
};


export default withContext(SimulationParameters);
