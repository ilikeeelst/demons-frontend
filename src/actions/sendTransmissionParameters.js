import {
    API_BASE_URL, SIMULATION_ENDPOINT, TESTBED_ENDPOINT, TRANSMISSION_LAUNCH_REQUEST_CREDENTIALS,
    TRANSMISSION_LAUNCH_REQUEST_HEADERS,
    TRANSMISSION_LAUNCH_REQUEST_METHOD, TRANSMISSION_LAUNCH_REQUEST_MODE
} from "../constants";
import {fetchUrl} from "./utils";


export default parameters => {
    console.log("Sending transmission parameters to the server...");
    let envirorment = parameters.environment;
    let endpoint;
    if (envirorment === 'simulation') {
        endpoint = SIMULATION_ENDPOINT
    }
    if (envirorment === 'testbed') {
        endpoint = TESTBED_ENDPOINT
    }
    let body = JSON.stringify(parameters);
    console.log(body);
    const fetchOptions = {
        method: TRANSMISSION_LAUNCH_REQUEST_METHOD,
        headers: TRANSMISSION_LAUNCH_REQUEST_HEADERS,
        mode: TRANSMISSION_LAUNCH_REQUEST_MODE,
        credentials: TRANSMISSION_LAUNCH_REQUEST_CREDENTIALS,
        body: body
    };
    return fetchUrl(API_BASE_URL + endpoint, fetchOptions)
        .then(response => {
            if (response.status !== 200) {
                console.warn("Error while fetching results")
            }
            return response.body
        })
        .catch((err => {
                console.warn("Error while fetching results");
                console.error(err);
                return {
                    success: false
                }
            }
        ))
}