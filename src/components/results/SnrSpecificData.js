import React, {Fragment} from 'react';
import  { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import withStyles from '@material-ui/core/styles/withStyles';
import Plot from "react-plotly.js";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import Button from "@material-ui/core/Button/Button";
import {dataDescriptions, snrSpecificDataFields} from "../../constants";

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    icon: {
        marginRight: theme.spacing.unit * 2,
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
        alignSelf: 'center'
    },
    cardGrid: {
        padding: `${theme.spacing.unit * 8}px 0`,
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 6,
    },
});

const SnrSpecificData = props => {
    const { classes } = props;
    const { channel, outputImage } = props.data;

    return (
        <Fragment>
            <main>
                <div className={classNames(classes.layout, classes.cardGrid)}>
                    {/* End hero unit */}
                    <Grid container spacing={40}>
                        <Grid item xs={20}>
                            <Card className={classes.card}>
                                <img src={"data:image/jpeg;base64, " + outputImage}/>
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="headline" component="h2">
                                        Output image
                                    </Typography>
                                    <Typography>
                                        This is how the image looks like after being transmitted
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={20}>
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Metric</TableCell>
                                        <TableCell>Value</TableCell>
                                        <TableCell>Info</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {snrSpecificDataFields.map(field => {
                                        return (
                                            <TableRow key={field}>
                                                <TableCell>{field}</TableCell>
                                                <TableCell>{props.data[field].toFixed(2)}</TableCell>
                                                <TableCell>{dataDescriptions[field].short}</TableCell>
                                                <TableCell>
                                                    <Button
                                                        variant="outlined"
                                                        color="primary"

                                                    >
                                                        More info
                                                    </Button>
                                                </TableCell>
                                            </TableRow>
                                        );
                                    })}
                                </TableBody>
                            </Table>
                        </Grid>
                        <Grid item xs={40}>
                            <Card className={classes.card}>
                                <Plot
                                    data={[
                                        {
                                            type: 'surface',
                                            x: channel.timeValues,
                                            y: channel.frequencyValues,
                                            z: channel.amplitudes}
                                    ]}
                                    layout={ {
                                        width: 800,
                                        height: 600,
                                        title: 'Channel',
                                        scene: {
                                            xaxis: {
                                                title:'Time (ms)'
                                            },
                                            yaxis: {
                                                title: 'Frequency (MHz)'
                                            },
                                            zaxis: {
                                                title: 'Amplitude'
                                            }
                                        },
                                    } }
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="headline" component="h2">
                                        Channel
                                    </Typography>
                                    <Typography>
                                        This interactive 3d graph represents the channel used for the transmission
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </div>
            </main>
        </Fragment>
    )
};

SnrSpecificData.propTypes = {
    data: PropTypes.object.isRequired
};

export default withStyles(styles)(SnrSpecificData);
