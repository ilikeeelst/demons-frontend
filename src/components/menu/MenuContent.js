import React, {Fragment} from 'react';
import { Link } from 'react-router-dom';
import withStyles from '@material-ui/core/styles/withStyles'
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import BurstMode from '@material-ui/icons/BurstMode';
import Home from '@material-ui/icons/Home';
import Help from '@material-ui/icons/Help';
import {DARK_GUNMETAL} from "../../constants";
import {SPARTAN_CRIMSON} from "../../constants";

const styles = () => ({
    root: {
        background: "linear-gradient(to bottom right, darkred,grey)",
    },
    menuButton: {
        background: `linear-gradient(to right, ${DARK_GUNMETAL},${SPARTAN_CRIMSON})`,
    },
    icon: {
        color: SPARTAN_CRIMSON
    }
});


const MenuItems = props => {

    const { classes } = props;

    return (
        <Fragment>
            <List>
                <Link to="/" className="link">
                    <ListItem button classes={{
                        root: classes.menuButton
                    }}>
                        <ListItemIcon classes={{
                            root: classes.icon
                        }}>
                            <Home/>
                        </ListItemIcon>
                        <ListItemText primary="Home" />
                    </ListItem>
                </Link>
            </List>
            <List>
                <Link to="/transmission" className="link">
                    <ListItem button classes={{
                        root: classes.menuButton
                    }}>
                        <ListItemIcon classes={{
                            root: classes.icon
                        }}>
                            <BurstMode/>
                        </ListItemIcon>
                        <ListItemText primary="Transmission" />
                    </ListItem>
                </Link>
            </List>
            <Divider />
            <List>
                <Link to="/tutorial" className="link">
                    <ListItem button classes={{
                        root: classes.menuButton
                    }}>
                        <ListItemIcon classes={{
                            root: classes.icon
                        }}>
                            <Help/>
                        </ListItemIcon>
                        <ListItemText primary="Tutorial" />
                    </ListItem>
                </Link>
            </List>
        </Fragment>
    )
};

export default withStyles(styles)(MenuItems);