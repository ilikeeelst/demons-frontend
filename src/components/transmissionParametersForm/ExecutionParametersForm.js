import React from 'react';
import { Link } from 'react-router-dom';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography'
import ImageCarousel from '../imagecarousel/ImageCarousel';
import PictureShooter from './PictureShooter';
import TestBedParameters from './TestBedParameters';
import SimulationParameters from './SimulationParameters';
import fetchImages from '../../actions/fetchImages'
import './ExecutionParametersForm.css'
import {generateQueryString} from "../../utils";


const environments = ["simulation", "testbed"];
const channelTypeOptions = ["itur3GIAx", "itur3GIBx"];
const simulationTransmissionTypeOptions = ["static", "quasi-static", "pedestrian"];
const simulationParameters = ["snr", "channelType", "transmissionType"];
const testbedParameters = ["txGain", "rxGain", "carrierFrequency"];


class ExecutionParametersForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            snr: 10.0,
            compareWithDigital: false,
            environment: environments[0],
            testBedIsAvailable: true,
            isLoading: false,
            error: null,
            images:[],
            selectedImageIndex: 0,
            imageConfirmed: false,
            webcamIsOpen: false,
            capturedImage: null,
            txGain: 0,
            rxGain: 0,
            channelType: channelTypeOptions[0],
            transmissionType: simulationTransmissionTypeOptions[0],
            carrierFrequency: 80,
            galleryIsOpen: false
        };
    }

    getParameters () {
        if (this.state.images.length === 0) {
            return {}
        }
        else {
            let environmentParameters;
            if (this.state.environment === "simulation") {
                environmentParameters = simulationParameters;
            }
            else {
                environmentParameters = testbedParameters;
            }
            let parameters = {
                environment: this.state.environment,
                imageId: this.state.images[this.state.selectedImageIndex].id,
                compareWithDigital: this.state.compareWithDigital,
            };
            for (let key of environmentParameters) {
                parameters[key] = this.state[key];
            }
            return parameters
        }

    }

    componentDidMount() {
        this.setState({isLoadingImages: true});
        fetchImages()
            .then(images => {
                this.setState({
                    images: images,
                    isLoadingImages: false
                });
            })
            .catch(error => {
                this.setState(
                    {
                        error: error,
                        isLoadingImages: false
                    }
                );
                console.log(error);
                }
            )
    }

    handleSnrChange(event) {
        this.setState({
            snr: event.target.value
        });
    }

    handleEnvironmentChange(event) {
        this.setState({
            environment: event.target.value
        })
    }

    handleTransmissionTxGainChange(event) {
        this.setState({
            txGain: event.target.value
        });
    }

    handleTransmissionRxGainChange(event) {
        this.setState({
            rxGain: event.target.value
        });
    }

    handleTransmissionTypeChange(event) {
        this.setState({
           transmissionType: event.target.value
        });
    }

    handleChannelTypeChange(event) {
        this.setState({
            channelType: event.target.value
        });
    }

    handleCarrierFrequencyChange(event) {
        this.setState({
           carrierFrequency: event.target.value
        });
    }

    toggleDigitalComparison() {
        this.setState({
            compareWithDigital: !this.state.compareWithDigital
        })
    }

    getCurrentImageIndex(i) {
        this.setState(
            {
                selectedImageIndex: i
            }
        )
    }

    toggleCamera () {
        if (this.state.capturedImage == null) {
            this.setState({
                webcamIsOpen: !this.state.webcamIsOpen,
                galleryIsOpen: false
            });
        }
        else {

        }
    }

    storeImage (image) {
        this.setState({
            capturedImage: image
        });
    }

    toggleOpenGallery() {
        this.setState({
            galleryIsOpen: !this.state.galleryIsOpen,
            webcamIsOpen: false,
        })
    }


    render() {
        let environmentParametersSelector;
        let galleryButtonText;
        let webcamButtonText;
        if (this.state.webcamIsOpen) {
            if (this.state.capturedImage != null) {
                webcamButtonText = "Take another picture"
            }
            else {
                webcamButtonText = "Close Webcam";
            }
        }
        else {
            webcamButtonText = "Open Webcam";
        }
        if (this.state.galleryIsOpen) {
            galleryButtonText = "Close Gallery";
        }
        else {
            galleryButtonText = "Open image gallery";
        }
        switch (this.state.environment) {
            case environments[0]:
                environmentParametersSelector = <SimulationParameters
                    onSnrChange={this.handleSnrChange.bind(this)}
                    onChannelTypeChange={this.handleChannelTypeChange.bind(this)}
                    channelTypeValue={this.state.channelType}
                    snrValue={this.state.snr}
                    channelTypeOptions={channelTypeOptions}
                    transmissionTypeValue={this.state.transmissionType}
                    onTransmissionTypeChange={this.handleTransmissionTypeChange.bind(this)}
                    transmissionTypeOptions={simulationTransmissionTypeOptions}
                />;
                break;
            case environments[1]:
                environmentParametersSelector= <TestBedParameters
                    onTransmissionGainTxChange={this.handleTransmissionTxGainChange.bind(this)}
                    onTransmissionGainRxChange={this.handleTransmissionRxGainChange.bind(this)}
                    onCarrierFrequencyChange={this.handleCarrierFrequencyChange.bind(this)}
                    transmissionGainTxValue={this.state.txGain}
                    transmissionGainRxValue={this.state.rxGain}
                    carrierFrequency={this.state.carrierFrequency}

                />;
                break;
            default:
                environmentParametersSelector= null
        }
        return (
                <form className="content1">
                    <Typography variant="headline">
                        Choose the parameters of the execution
                    </Typography>
                    <FormControl component="div" className="environmentSelector">
                        <FormLabel component="legend">Choose an Environment:</FormLabel>
                        <RadioGroup
                            aria-label="Environment"
                            name="Environment"
                            value={this.state.environment}
                            onChange={this.handleEnvironmentChange.bind(this)}
                            className="alignRight"
                        >
                            <FormControlLabel
                                value="simulation"
                                control={<Radio />}
                                label="Simulation"/>
                            <FormControlLabel
                                value="testbed"
                                disabled={!this.state.testBedIsAvailable}
                                control={<Radio />}
                                label="Testbed"/>
                        </RadioGroup>
                    </FormControl>
                    {environmentParametersSelector}
                    <div>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={this.state.compareWithDigital}
                                    onChange={this.toggleDigitalComparison.bind(this)}
                                    value={this.state.compareWithDigital}
                                />
                            }
                            label="Compare with digital"
                        />
                    </div>
                    <FormLabel component="legend" className="form-element">
                        Choose the image that will be used for the transmission
                    </FormLabel>
                    <Button variant="outlined" onClick={this.toggleOpenGallery.bind(this)}>
                        {galleryButtonText}
                    </Button>
                    <ImageCarousel
                        images={this.state.images}
                        loading={this.state.isLoadingImages}
                        onSlideChange={this.getCurrentImageIndex.bind(this)}
                        open={this.state.galleryIsOpen}
                    />
                    <p>
                        Or
                    </p>
                    <Button variant="outlined"
                            onClick={this.toggleCamera.bind(this)}
                    >
                        {webcamButtonText}
                    </Button>
                    <PictureShooter onConfirm={this.storeImage.bind(this)} open={this.state.webcamIsOpen}/>
                    <div className="form-element">
                        <Button
                            type="submit"
                            color="primary"
                            variant="raised"
                            disabled={this.state.capturedImage == null
                            && (!this.state.galleryIsOpen
                                || this.state.images.length < 1)}
                            component={
                                props => <Link to={{
                                    pathname: "/results",
                                    search: generateQueryString(this.getParameters())
                                }} {...props}/>}
                        >
                            BEGIN TRANSMISSION
                        </Button>
                    </div>
                </form>
        )
    }
}

ExecutionParametersForm.propTypes = {
};

export default ExecutionParametersForm;
