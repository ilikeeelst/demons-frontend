import React from "react";
import PropTypes from 'prop-types';
import { withContext } from "../../context";
import SimulationParameters from './SimulationParameters';
import TestBedParameters from './TestBedParameters';
const EnvironmentParametersForm = props => {
    let parametersForm;
    if (props.environment === "simulation") {
        parametersForm =
            <SimulationParameters/>
    }
    else {
        parametersForm =
            <TestBedParameters/>
    }
    return (
        <React.Fragment>
            {parametersForm}
        </React.Fragment>
    )
};

EnvironmentParametersForm.propTypes = {
    classes: PropTypes.object
};

export default withContext(EnvironmentParametersForm);