import React from 'react';
import PropTypes from 'prop-types';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {withContext} from "../../context";


const LogoutDialog = props => {
    return (
        <Dialog open={props.logoutDialogIsOpen} onClose={props.closeLogoutDialog}>
            <DialogTitle>
                Are you sure?
            </DialogTitle>
            <DialogActions>
                <Button onClick={props.closeLogoutDialog} color="primary">
                    Cancel
                </Button>
                <Button onClick={props.handleLogout} color="primary">
                    Confirm
                </Button>
            </DialogActions>
        </Dialog>
    )
};

LogoutDialog.PropTypes =  {
    closeLogoutDialog: PropTypes.func.isRequired,
    handleLogout: PropTypes.func.isRequired,
    logoutDialogIsOpen: PropTypes.bool.isRequired
};

export default withContext (LogoutDialog);