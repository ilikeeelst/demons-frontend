import {
    API_BASE_URL,
    IMAGES_ENDPOINT,
    UPLOAD_IMAGE_CREDENTIALS,
    IMAGE_UPLOAD_HEADERS,
    IMAGE_UPLOAD_METHOD,
    IMAGE_UPLOAD_MODE,
} from "../constants";
import {fetchUrl} from "./utils";


export default (imageName, imageFile) => {
    console.log("uploading image...");
    console.log(imageFile);
    let reader = new FileReader();
    let imageData = null;
    reader.onload = (e) => {
        imageData = e.target.result;
        const fetchOptions = {
            method: IMAGE_UPLOAD_METHOD,
            headers: IMAGE_UPLOAD_HEADERS,
            body: JSON.stringify({
                "name": imageName,
                "base64EncodedImage": btoa(imageData)
            }),
            mode: IMAGE_UPLOAD_MODE,
            credentials: UPLOAD_IMAGE_CREDENTIALS
        };
        return fetchUrl(API_BASE_URL + IMAGES_ENDPOINT, fetchOptions)
            .then( response => {
                if (response.status !== 201) {
                    console.warn("The server refuses upload the image. STATUS: " + response.status)
                }
                console.log("Image uploaded successfully");
                return response.body;
            })
            .then(responseBody => {
                return responseBody.created
            })
            .catch( err => {
                console.error(err);
                return false
            })
    };
    reader.readAsText(imageFile);


};
