import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';
import Pageview from '@material-ui/icons/Pageview';
import MethodDataDialog from './MethodDataDialog';
import {RUBY_RED} from "../../constants";
import getTransmissionDataAction from '../../actions/getTransmissionDataAction';
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";

const styles = theme => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    toolbarMain: {
        borderBottom: `1px solid ${theme.palette.grey[300]}`,
    },
    toolbarTitle: {
        flex: 1,
    },
    toolbarSecondary: {
        justifyContent: 'space-between',
    },
    mainFeaturedPost: {
        backgroundColor: theme.palette.grey[800],
        color: theme.palette.common.white,
        marginBottom: theme.spacing.unit * 4,
    },
    mainFeaturedPostContent: {
        padding: `${theme.spacing.unit * 6}px`,
        [theme.breakpoints.up('md')]: {
            paddingRight: 0,
        },
    },
    mainGrid: {
        marginTop: theme.spacing.unit * 3,
    },
    card: {
        display: 'flex',
    },
    cardDetails: {
        flex: 1,
    },
    exploreButton: {
        width: 160,
        backgroundColor: RUBY_RED
    },
    exploreIcon: {
    },
    markdown: {
        padding: `${theme.spacing.unit * 3}px 0`,
    },
    sidebarAboutBox: {
        padding: theme.spacing.unit * 2,
        backgroundColor: theme.palette.grey[200],
    },
    sidebarSection: {
        marginTop: theme.spacing.unit * 3,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        marginTop: theme.spacing.unit * 8,
        padding: `${theme.spacing.unit * 6}px 0`,
    },
});

const sections = [
];


class TransmissionResultsPage extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            isLoadingImages: true,
            data: {},
            dataDialogIsOpen: false,
            selectedMethod: 0
        }
    }

    componentDidMount() {
        getTransmissionDataAction(this.props.match.params.id)
            .then( data => {
                console.log(data);
                this.setState({
                    data: data,
                    isLoadingImages: false
                });
            })
    }

    openDataDialog() {
        this.setState({
            dataDialogIsOpen: true
        })
    }

    closeDataDialog() {
        this.setState({
            dataDialogIsOpen: false
        })
    }

     render () {
        const {classes} = this.props;

        let content;

        if (this.state.isLoadingImages) {
            content = <CircularProgress size="50%" color="primary"/>
        }
        else {
            let cards = [];
            for (let item of this.state.data.methods.entries()) {
                let index = item[0];
                let method = item[1];
                cards.push(
                    <Grid item key={index} xs={12} md={6}>
                        <Card className={classes.card}>
                            <div className={classes.cardDetails}>
                                <CardContent>
                                    <Typography variant="headline">Method {index+1}</Typography>
                                    {method.strategy.map(section => (
                                        <Typography variant="subheading">
                                            {section}
                                        </Typography>
                                    ))}
                                    <Button variant="outlined" onClick={() => {
                                        this.setState({
                                            selectedMethod: index
                                        });
                                        this.openDataDialog.bind(this)()
                                    }}>
                                        Click here (or on the red button) for more details
                                    </Button>
                                </CardContent>
                            </div>
                            <Hidden xsDown>
                                <Button className={classes.exploreButton}
                                        onClick={() => {
                                            this.setState({
                                               selectedMethod: index
                                            });
                                            this.openDataDialog.bind(this)()
                                        }}>
                                    <Pageview className={classes.exploreIcon}/>
                                </Button>
                            </Hidden>
                        </Card>
                    </Grid>
                )
            }
            content =
                <div className={classes.layout}>
                    <MethodDataDialog
                        open={this.state.dataDialogIsOpen}
                        onClose={this.closeDataDialog.bind(this)}
                        data={this.state.data.methods[this.state.selectedMethod]}
                    />
                    <Toolbar variant="dense" className={classes.toolbarSecondary}>
                        {sections.map(section => (
                            <Typography color="inherit" noWrap key={section}>
                                {section}
                            </Typography>
                        ))}
                    </Toolbar>
                    <main>
                        <Paper className={classes.mainFeaturedPost}>
                            <Grid container>
                                <Grid item>
                                    <div className={classes.mainFeaturedPostContent}>
                                        <Typography variant="display2" gutterBottom>
                                            Results of the simulation
                                        </Typography>
                                        <Typography variant="headline" color="inherit" paragraph>
                                            Here below you will find the data collected during the simulation.
                                            Click on the explore button to view them in detail!
                                        </Typography>

                                    </div>
                                </Grid>
                            </Grid>
                        </Paper>
                        <Grid container spacing={40} className={classes.cardGrid}>
                            {cards}
                        </Grid>
                    </main>
                </div>
        }
        return (
             <React.Fragment>
                 <CssBaseline />

                 {content}
             </React.Fragment>
         );
     }


}

TransmissionResultsPage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TransmissionResultsPage);