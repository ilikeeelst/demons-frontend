export const API_BASE_URL = 'http://localhost:8080';
export const LOGIN_ENDPOINT = '/api/v1/auth/login';
export const LOGIN_REQUEST_METHOD = 'POST';
export const LOGIN_REQUEST_HEADERS = {
    "Content-Type": "application/json; charset=utf-8"
};
export const LOGIN_REQUEST_MODE = 'cors';
export const LOGIN_REQUEST_CREDENTIALS = 'same-origin';
export const TOKEN_HEADER_NAME = "Authorization";
export const TOKEN_PREFIX = "Bearer";
export const TOKEN_FIELD_NAME = 'accessToken';
export const SIGNUP_ENDPOINT = '/api/v1/auth/signup';
export const SIGNUP_REQUEST_METHOD = 'POST';
export const SIGNUP_REQUEST_HEADERS = {
    "Content-Type": "application/json; charset=utf-8"
};
export const SIGNUP_REQUEST_MODE = 'cors';
export const SIGNUP_REQUEST_CREDENTIALS = 'same-origin';

export const IMAGES_ENDPOINT = "/images";
export const IMAGE_DETAILS_ENDPOINT = "/images/image";
export const GET_IMAGES_LIST_METHOD = 'GET';
export const GET_IMAGES_LIST_MODE = 'cors';
export const GET_IMAGE_DETAIL_METHOD = 'GET';
export const GET_IMAGE_DETAIL_MODE = 'cors';
export const IMAGE_UPLOAD_METHOD = 'POST';
export const IMAGE_UPLOAD_MODE = 'cors';
export const IMAGE_UPLOAD_HEADERS = {
    "Content-Type": "application/json; charset=utf-8"
};
export const UPLOAD_IMAGE_CREDENTIALS = 'same-origin';


export const STORAGE_USERNAME_FIELD = 'username';

export const SIMULATION_ENDPOINT= '/api/v1/transmissions/new/simulation';
export const TESTBED_ENDPOINT = '/api/v1/transmissions/new/testbed';
export const TRANSMISSION_LAUNCH_REQUEST_METHOD= 'POST';
export const TRANSMISSION_LAUNCH_REQUEST_HEADERS= {
    "Content-Type": "application/json; charset=utf-8"
};
export const TRANSMISSION_LAUNCH_REQUEST_MODE = 'cors';
export const TRANSMISSION_LAUNCH_REQUEST_CREDENTIALS = 'same-origin';

export const TRANSMISSIONS_LIST_ENDPOINT= '/api/v1/transmissions';
export const TRANSMISSIONS_LIST_REQUEST_METHOD = 'GET';
export const TRANSMISSIONS_LIST_REQUEST_MODE = 'cors';
export const TRANSMISSIONS_LIST_REQUEST_CREDENTIALS = 'same-origin';

export const TRANSMISSION_ENDPOINT= '/api/v1/transmissions/transmission';
export const TRANSMISSION_METHOD = 'GET';
export const TRANSMISSION_MODE = 'cors';
export const TRANSMISSION_CREDENTIALS = 'same-origin';
//STYLES

export const RICH_BLACK = "#050505";
export const DARK_GUNMETAL = "#1c2826";
export const ROSEWOOD= "#690500";
export const RUBY_RED = "#91171f";
export const SPARTAN_CRIMSON = "#95190c";
export const PINK = "#755b69";
export const CITRINE = "#e3b505";


export const emailRegex = RegExp(
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);



export const enviromentParameters = {
    simulation: ["snr", "channelType", "transmissionType"],
    testbed: ["txGain", "rxGain", "carrierFrequency"]
};
export const parametersDescription = {
    simulation: "A simulated environment developed with Matlab",
    testbed: "Over-the-air transmission over a real channel",
    snr: "Approximated signal-to-noise ratio. (The calculated one will always be lower)",
    channelType: "The type of the channel that will by instantiated by Matlab",
    transmissionType: "The scenario of the transmission",
    txGain: '',
    rxGain: '',
    carrierFrequency: ''
};

export const environments = ["simulation", "testbed"];
export const channelTypes = ["itur3GIAx", "itur3GIBx"];
export const simulationTransmissionTypes = ["static", "quasi-static", "pedestrian"];
export const snrSpecificDataFields = ['calculatedSNR', 'ssim', 'sdr'];
export const dataDescriptions = {
    calculatedSNR: {
        short: 'The real SNR calculated during the transmission',
        page: 'https://en.wikipedia.org/wiki/Signal-to-noise_ratio'
    },
    ssim: {
        short: 'Structural Similarity Index',
        page: 'https://en.wikipedia.org/wiki/Structural_similarity',
    },
    sdr: {
        short: 'Signal-distortion-ratio',
        page: '',
    },
};


export const SNR_MIN_VAL = 0.0;
export const SNR_MAX_VAL = 100.0;
export const SNR_STEP = 0.1;

export const TX_GAIN_MIN_VAL = 0.0;
export const TX_GAIN_MAX_VAL = 100.0;
export const TX_GAIN_STEP = 0.1;

export const RX_GAIN_MIN_VAL = 0.0;
export const RX_GAIN_MAX_VAL = 100.0;
export const RX_GAIN_STEP = 0.1;

export const CARRIER_FREQUENCY_MIN_VAL = 80;
export const CARRIER_FREQUENCY_MAX_VAL = 6000;