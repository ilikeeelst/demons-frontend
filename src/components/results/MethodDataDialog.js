import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import SnrSpecificData from './SnrSpecificData';

const styles = {
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
    },
};

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

const MethodDataDialog = props => {

    const { classes } = props;

    return (
        <Dialog
            fullScreen
            open={props.open}
            onClose={props.onClose}
            TransitionComponent={Transition}
        >
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <Typography variant="title" color="inherit" className={classes.flex}>
                        Detailed data
                    </Typography>
                    <IconButton color="inherit" onClick={props.onClose}>
                        close
                        <CloseIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <List>
                {props.data.snrs.map( snr => (
                        <ListItem>
                            <SnrSpecificData data={snr}/>
                        </ListItem>
                    )
                )}
            </List>
        </Dialog>
    );
};

MethodDataDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired
};

export default withStyles(styles)(MethodDataDialog);
