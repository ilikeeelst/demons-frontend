import React from 'react';
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import Button from "@material-ui/core/Button/Button";
import TextField from "@material-ui/core/TextField"
import { withStyles } from '@material-ui/core/styles';
import fetchImages from "../../actions/fetchImages";
import uploadImageAction from '../../actions/uploadImageAction';

const styles = {
    root: {
        width: '100%',
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    textField: {
        width: 200,
    },
};



class AdminPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            images: [],
            imageName: "",
            imageFile: null
        }
    }

    componentDidMount() {
        fetchImages()
            .then(images => {
                this.setState({
                    images: images,
                    isLoading: false
                });
            })
            .catch(error => {
                    this.setState(
                        {
                            error: error,
                            isLoading: false
                        }
                    );
                    console.log(error);
                }
            )

    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    handleImageChange(event) {
        if (event.target.files && event.target.files[0]) {
            this.setState({
               imageFile: event.target.files[0]
            });
        }
    }

    render () {
        const { classes } = this.props;


        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell>Name</TableCell>
                            <TableCell>View</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.images.map(image => {
                            return (
                                <TableRow key={image.id}>
                                    <TableCell component="th" scope="row">
                                        {image.id}
                                    </TableCell>
                                    <TableCell>{image.name}</TableCell>
                                    <TableCell>
                                        <Button
                                            variant="outlined"
                                            color="primary"
                                        >
                                            Delete
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                <Button
                    containerElement='label' // <-- Just add me!
                    label='My Label'>
                    <input type="file" onChange={this.handleImageChange.bind(this)} accept="image/*"/>
                </Button>
                <TextField
                    id="imageName"
                    label="Image Name"
                    className={classes.textField}
                    value={this.state.imageName}
                    onChange={this.handleChange('imageName')}
                    margin="normal"
                />
                <Button onClick={() => {
                    this.setState({
                       isLoading: true
                    });
                    uploadImageAction(this.state.imageName, this.state.imageFile);
                    this.setState({
                        isLoading: false
                    });
                }}>
                    UPLOAD
                </Button>
            </Paper>
        )
    }
}

export default withStyles(styles)(AdminPage);