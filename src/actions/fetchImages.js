import {fetchUrl} from "./utils";
import {
    API_BASE_URL,
    IMAGES_ENDPOINT,
    GET_IMAGES_LIST_METHOD,
    GET_IMAGES_LIST_MODE} from "../constants";


export default () => {
    console.log("fetching images list...");
    const fetchOptions = {
        method: GET_IMAGES_LIST_METHOD,
        mode: GET_IMAGES_LIST_MODE
    };
    return fetchUrl(API_BASE_URL + IMAGES_ENDPOINT, fetchOptions)
        .then(response => {
            if (response.status === 200) {
                return response.body.images;
            }
            else {
                throw new Error("Something went wrong while fetching the images");
            }
        })
}