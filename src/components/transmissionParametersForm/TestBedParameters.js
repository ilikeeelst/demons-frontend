import React from 'react'
import PropTypes from 'prop-types'
import InputAdornment from '@material-ui/core/InputAdornment';
import Input from "@material-ui/core/Input";
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid/Grid";
import { withContext } from "../../context";
import {
    CARRIER_FREQUENCY_MAX_VAL,
    CARRIER_FREQUENCY_MIN_VAL,
    RX_GAIN_MAX_VAL,
    RX_GAIN_MIN_VAL,
    RX_GAIN_STEP,
    TX_GAIN_MAX_VAL,
    TX_GAIN_MIN_VAL,
    TX_GAIN_STEP
} from "../../constants";


const TestBedParameters = props => {
    return (
        <Grid container spacing={24}>
            <Grid item xs={12} sm={6}>
                <FormControl fullWidth>
                    <InputLabel>
                        Tx Gain
                    </InputLabel>
                    <Input
                        error={!props.txGainIsValid()}
                        id="txGain"
                        value={props.txGain}
                        onChange={event => {
                            props.onFieldChange("txGain", event)
                        }}
                        endAdornment={<InputAdornment position="end">dB</InputAdornment>}
                        type="number"
                        inputProps={{
                            step: TX_GAIN_STEP,
                            min: TX_GAIN_MIN_VAL,
                            max: TX_GAIN_MAX_VAL
                        }}
                    />
                </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormControl fullWidth>
                    <InputLabel>
                        Rx Gain
                    </InputLabel>
                    <Input
                        error={!props.rxGainIsValid()}
                        id="rxGain"
                        value={props.rxGain}
                        onChange={event => {
                            props.onFieldChange("rxGain", event)
                        }}
                        endAdornment={<InputAdornment position="end">dB</InputAdornment>}
                        type="number"
                        inputProps={{
                            step: RX_GAIN_STEP,
                            min: RX_GAIN_MIN_VAL,
                            max: RX_GAIN_MAX_VAL
                        }}
                    />
                </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormControl fullWidth>
                    <InputLabel>
                        Carrier Frequency
                    </InputLabel>
                    <Input
                        error={!props.carrierFrequencyIsValid()}
                        id="carrierFrequency"
                        value={props.carrierFrequency}
                        onChange={ event => {
                            props.onFieldChange("carrierFrequency", event)
                        }}
                        endAdornment={<InputAdornment position="end">MHz</InputAdornment>}
                        type="number"
                        inputProps={{
                            min: CARRIER_FREQUENCY_MIN_VAL,
                            max: CARRIER_FREQUENCY_MAX_VAL
                        }}
                    />
                </FormControl>
            </Grid>
        </Grid>
    )
};

TestBedParameters.propTypes = {
};


export default withContext(TestBedParameters);
