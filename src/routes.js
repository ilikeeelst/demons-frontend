import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import Welcome from "./components/home/Welcome";
import LoggedUserHome from "./components/home/LoggedUserHome";
import ResultsPage from "./components/results/TransmissionResultsPage";
import Form from './components/transmissionParametersForm/TransmissionSteps';
import AdminPage from './components/adminpanel/AdminPage';


export const paths = {
    home: "/",
    tutorial: "/tutorial",
    transmission: "/transmission",
    results: "/results/:id",
    admin: "/admin"
};

export const getHomeRoute = (props) =>
    <Route
        path={paths.home}
        render={ () => {
            if (props.isLoggedIn) {
                return <LoggedUserHome { ...props} />
            }
            else {
                return <Welcome {...props} />
            }
        }}
    />;

export const getTutorialRoute = () =>
    <Route
        exact path={paths.tutorial}
    />;

export const getTransmissionRoute = authorized =>
    <Route
        path={paths.transmission}
        render={ props => {
            if(authorized) {
                return <Form {...props}/>
            }
            else{
                return <Redirect to={paths.home}/>
            }
        }}
    />;

export const getTransmissionResultsRoute = authorized =>
    <Route
        path={paths.results}
        render={ props => {
            if(authorized) {
                return <ResultsPage {...props}/>
            }
            else{
                return <Redirect to={paths.home}/>
            }
        }}/>;


export const getAdminRoute = authorized =>
    <Route
        path={paths.admin}
        render={ props => {
            if(authorized) {
                return <AdminPage {...props}/>
            }
            else{
                return <Redirect to={paths.home}/>
            }
        }}/>;