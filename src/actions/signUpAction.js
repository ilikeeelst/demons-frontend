import {
    API_BASE_URL,
    SIGNUP_ENDPOINT,
    SIGNUP_REQUEST_CREDENTIALS,
    SIGNUP_REQUEST_HEADERS,
    SIGNUP_REQUEST_METHOD,
    SIGNUP_REQUEST_MODE
} from "../constants";
import {fetchUrl} from "./utils";


export default (email, password) => {
    console.log("Sending registration request...");
    const fetchOptions = {
        method: SIGNUP_REQUEST_METHOD,
        headers: SIGNUP_REQUEST_HEADERS,
        body: JSON.stringify({
            "email": email,
            "password": password
        }),
        mode: SIGNUP_REQUEST_MODE,
        credentials: SIGNUP_REQUEST_CREDENTIALS
    };
    return fetchUrl(API_BASE_URL + SIGNUP_ENDPOINT, fetchOptions)
        .then( response => {
            if (response.status !== 201) {
                console.warn("The server refuses to create the user. STATUS: " + response.status)
            }
            return response.body;
        })
        .then(responseBody => {
            return responseBody.created
        })
        .catch( err => {
            console.error(err);
            return false
        })
};
