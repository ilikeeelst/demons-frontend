import React from 'react';
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {withContext} from "../../context";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    toolbarTitle: {
        flex: 1,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },
    heroButtons: {
        marginTop: theme.spacing.unit * 4,
    },
    cardHeader: {
        backgroundColor: theme.palette.grey[200],
    },
    cardPricing: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'baseline',
        marginBottom: theme.spacing.unit * 2,
    },
    cardActions: {
        [theme.breakpoints.up('sm')]: {
            paddingBottom: theme.spacing.unit * 2,
        },
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    cardGrid: {
        padding: `${theme.spacing.unit * 8}px 0`,
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    exploreButton: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
});


const cards = [
    {
        imageUrl: "https://upload.wikimedia.org/wikipedia/commons/6/63/Dct-table.png",
        title: "Image transmission",
        description: "Start experimenting with image analog transmission, we will take care of the technical parts for you!"
    },
    {
        imageUrl: "http://portlandmetrolive.com/wp-content/uploads/2016/07/main_free.jpg",
        title: "For free",
        description: "Demons is completely free to use, create a new acccount now!"
    },
    {
        imageUrl: "https://caktus-website-production-2015.s3.amazonaws.com/media/blog-images/responsive-web-design-caktus.gif",
        title: "Responsive",
        description: "Demons adapts to different screen sizes, You should be able to use it with your mobile just fine!"
    },
    {
        imageUrl: "https://www.vectorlogo.es/wp-content/uploads/2014/12/logo-vector-universidade-da-coruna.jpg",
        title: "Made in UDC",
        description: "Developed by GTEC, a research group of Univesidade Da Coruña "
    }

    ];

function Welcome (props) {
    const { classes } = props;

    return (
        <React.Fragment>
            <CssBaseline />
            <main className={classes.layout}>
                {/* Hero unit */}
                <div className={classes.heroContent}>
                    <Typography variant="display3" align="center" color="textPrimary" gutterBottom>
                        Introducing Demons
                    </Typography>
                    <Typography variant="title" align="center" color="textSecondary" component="p">
                        Demons is an open source web app that allows you to run experiments on analog image transmissions.
                        <br/>
                        Demons is easy to use. Start using it now!
                    </Typography>
                    <div className={classes.heroButtons}>
                        <Grid container spacing={16} justify="center">
                            <Grid item>
                                <Button variant="raised" color="primary" onClick={props.openSignupDialog}>
                                    Sign up for free
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                    <div>
                        <Grid item>
                            <Typography variant="subheading" align="center">
                                Already registered?
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Button variant="contained" color="primary" onClick={props.openLoginDialog}>
                                Log in
                            </Button>
                        </Grid>
                    </div>
                </div>
                {/* End hero unit */}
                <div className={classNames(classes.layout, classes.cardGrid)}>
                    {/* End hero unit */}
                    <Grid container spacing={40}>
                        {cards.map(card => (
                            <Grid item key={card} sm={6} md={4} lg={3}>
                                <Card className={classes.card}>
                                    <CardMedia
                                        className={classes.cardMedia}
                                        image={card.imageUrl}
                                        title="Image title"
                                    />
                                    <CardContent className={classes.cardContent}>
                                        <Typography gutterBottom variant="headline" component="h2">
                                            {card.title}
                                        </Typography>
                                        <Typography>
                                            {card.description}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                </div>
            </main>
        </React.Fragment>
    );
}

Welcome.propTypes = {
    classes: PropTypes.object.isRequired,
    openLoginDialog: PropTypes.func.isRequired,
    openSignupDialog: PropTypes.func.isRequired,
};


export default withContext(withStyles(styles)(Welcome));