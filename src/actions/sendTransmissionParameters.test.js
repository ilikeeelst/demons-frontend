import React from 'react'
import {generateQueryString} from "../utils";

describe("the generateQueryString function", () => {
    it("returns an empy string if an empty object is passed", async () => {
        expect(generateQueryString({})).toEqual("")
    });
    it("returns the correct output if the input contains only one parameter", async () => {
       expect(generateQueryString({a: "a"})).toEqual("?a=a")
    });
    it("returns the correct output if the input contains more than one parameter", async ()=> {
       expect(generateQueryString({a: "a", b: "b", c: "c"})).toEqual("?a=a&b=b&c=c");
    });
});