import React from 'react';
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import FormHelperText from '@material-ui/core/FormHelperText';
import './Dialogs.css';
import { withContext } from '../../context';


const SignupDialogContent = props => {

    let emailIsValid = props.inputEmailIsValid();
    let passwordIsValid = props.inputPasswordIsValid();
    let confirmationPasswordIsValid = props.inputConfirmationPasswordIsValid();

    let emailHelperText = emailIsValid ? ""
        : "Doesn't seem to be a valid email address!";
    let passwordHelperText = passwordIsValid? "" : "This is not a valid Password";
    let confirmationPasswordHelperText = confirmationPasswordIsValid ?
        "" : "Please repeat here the exact same Password";

    return (
            <DialogContent>
                <DialogContentText>
                    Register a new account to this website!
                </DialogContentText>
                <div>
                    <TextField
                        label="Email Address"
                        type="email"
                        value={props.inputEmail}
                        onChange={props.onInputEmailChange}
                        error={!emailIsValid}
                    />
                    <FormHelperText id="name-error-text">{emailHelperText}</FormHelperText>
                </div>
                <div>
                    <TextField
                        label="Password"
                        type="password"
                        value={props.inputPassword}
                        onChange={props.onInputPasswordChange}
                        error={!passwordIsValid}
                    />
                    <FormHelperText id="name-error-text">{passwordHelperText}</FormHelperText>
                </div>
                <div>
                    <TextField
                        label="Confirm Password"
                        type="password"
                        value={props.inputConfirmationPassword}
                        onChange={props.onInputConfirmationPasswordChange}
                        error={!confirmationPasswordIsValid}
                    />
                    <FormHelperText id="name-error-text">{confirmationPasswordHelperText}</FormHelperText>
                </div>
            </DialogContent>
    );
};

SignupDialogContent.propTypes = {
    inputEmail: PropTypes.string.isRequired,
    onInputEmailChange: PropTypes.func.isRequired,
    inputEmailIsValid: PropTypes.func.isRequired,
    inputPassword: PropTypes.string.isRequired,
    onInputPasswordChange: PropTypes.func.isRequired,
    inputPasswordIsValid: PropTypes.func.isRequired,
    inputConfirmationPassword: PropTypes.string.isRequired,
    onInputConfirmationPasswordChange: PropTypes.func.isRequired,
    inputConfirmationPasswordIsValid: PropTypes.func.isRequired
};

export default withContext(SignupDialogContent);

