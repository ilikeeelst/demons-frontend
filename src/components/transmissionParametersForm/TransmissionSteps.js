import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import EnvironmentStep from './EnvironmentStep';
import PaymentForm from './ImageSelection';
import Review from './Review';
import fetchImages from '../../actions/fetchImages';
import EnvironmentParametersForm from './EnvironmentParametersForm';
import { Provider } from '../../context';
import {
    environments,
    channelTypes,
    simulationTransmissionTypes,
    SNR_MIN_VAL,
    SNR_MAX_VAL,
    TX_GAIN_MIN_VAL,
    TX_GAIN_MAX_VAL,
    RX_GAIN_MAX_VAL,
    RX_GAIN_MIN_VAL,
    CARRIER_FREQUENCY_MIN_VAL,
    CARRIER_FREQUENCY_MAX_VAL, enviromentParameters
} from '../../constants';
import sendTransmissionParameters from "../../actions/sendTransmissionParameters";
import uploadImageAction from '../../actions/uploadImageAction';

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 2,
        marginRight: theme.spacing.unit * 2,
        [theme.breakpoints.up(600 + theme.spacing.unit * 2 * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 3,
        marginBottom: theme.spacing.unit * 3,
        padding: theme.spacing.unit * 2,
        [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
            marginTop: theme.spacing.unit * 6,
            marginBottom: theme.spacing.unit * 6,
            padding: theme.spacing.unit * 3,
        },
    },
    stepper: {
        padding: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 5}px`,
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing.unit * 3,
        marginLeft: theme.spacing.unit,
    },
});

const steps = ['Environment', 'Image', 'Review'];


class TransmissionSteps extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activeStep: 0,
            snr: 10.0,
            compareWithDigital: false,
            environment: environments[0],
            testBedIsAvailable: true,
            isLoadingImages: false,
            error: null,
            images: [],
            selectedImageIndex: 0,
            imageConfirmed: false,
            webcamIsOpen: false,
            capturedImage: null,
            txGain: 0,
            rxGain: 0,
            channelType: channelTypes[0],
            transmissionType: simulationTransmissionTypes[0],
            carrierFrequency: 80,
            galleryIsOpen: true
        };
        this.handleFieldChange = this.handleFieldChange.bind(this);
    }

    componentDidMount() {
        window.scrollTo(0,0);
        this.setState({isLoadingImages: true});
        fetchImages()
            .then(images => {
                this.setState({
                    images: images,
                    isLoadingImages: false
                });
            })
            .catch(error => {
                    this.setState(
                        {
                            error: error,
                            isLoadingImages: false
                        }
                    );
                    console.log(error);
                }
            )
    }

    getContext = () => ({
        environment: this.state.environment,
        compareWithDigital: false,
        snr: this.state.snr,
        channelType: this.state.channelType,
        transmissionType: this.state.transmissionType,
        txGain: this.state.txGain,
        rxGain: this.state.rxGain,
        carrierFrequency: this.state.carrierFrequency,
        onFieldChange: this.handleFieldChange,
        snrIsValid: this.snrIsValid,
        txGainIsValid: this.txGainIsValid,
        rxGainIsValid: this.rxGainIsValid,
        carrierFrequencyIsValid: this.carrierFrequencyIsValid,
        images: this.state.images,
        selectedImageIndex: this.state.selectedImageIndex,
        isLoadingImages: this.state.isLoadingImages,
        onSlideChange: this.setCurrentImageIndex.bind(this),
        toggleCamera: this.toggleCamera,
        galleryIsOpen: this.state.galleryIsOpen,
        imageConfirmed: this.state.imageConfirmed,
        webcamIsOpen: this.state.webcamIsOpen,
        capturedImage: this.state.capturedImage,
        storeImage: this.storeImage
    });

    toggleCamera = () => {
        window.scrollTo(0,0);
        this.setState({
            webcamIsOpen: !this.state.webcamIsOpen,
            galleryIsOpen: !this.state.galleryIsOpen
        })
    };

    storeImage = (image) => {
        let imageSrc = image.split(",")[1];
        console.log(imageSrc);
        uploadImageAction('', imageSrc);
        this.setState({
            capturedImage: image
        });
    };

    handleFieldChange (field, event) {
        this.setState({
            [field]: event.target.value
        });
    }

    getStepContent(step) {
        switch (step) {
            case 0:
                return (
                    <EnvironmentStep
                        testBedIsAvailable={this.state.testBedIsAvailable}
                        environment={this.state.environment}
                        onEnvironmentChange={this.handleEnvironmentChange.bind(this)}
                    >
                        <EnvironmentParametersForm/>
                    </EnvironmentStep>
                ) ;
            case 1:
                return <PaymentForm/>;
            case 2:
                return <Review/>;
            default:
                throw new Error('Unknown step');
        }
    }

    handleEnvironmentChange(event) {
        console.log("Switching environment...");
        this.setState({
            environment: event.target.value
        })
    }

    handleNext = () => {
        window.scrollTo(0,0);
        const {activeStep} = this.state;
        if ( this.dataAreValid()) {
            this.setState({
                activeStep: activeStep + 1,
            });

        }
        else {

        }
        if (activeStep === steps.length - 1 && this.dataAreValid) {
            let parameters = {
                environment: this.state.environment,
                compareWithDigital: this.state.compareWithDigital,
                imageId: this.state.images[this.state.selectedImageIndex].id
            };
            for (let param of enviromentParameters[this.state.environment]) {
                parameters[param] = this.state[param]
            }
            sendTransmissionParameters(parameters)
        }
    };

    handleBack = () => {
        const {activeStep} = this.state;
        this.setState({
            activeStep: activeStep - 1,
        });
    };

    handleReset = () => {
        this.setState({
            activeStep: 0,
        });
    };

    snrIsValid = () => {
        return this.state.snr >= SNR_MIN_VAL && this.state.snr <= SNR_MAX_VAL
    };

    txGainIsValid = () => {
        return this.state.txGain >= TX_GAIN_MIN_VAL && this.state.txGain <= TX_GAIN_MAX_VAL
    };

    rxGainIsValid = () => {
        return this.state.rxGain >= RX_GAIN_MIN_VAL && this.state.rxGain <= RX_GAIN_MAX_VAL
    };

    carrierFrequencyIsValid = () => {
        return this.state.carrierFrequency >= CARRIER_FREQUENCY_MIN_VAL
            && this.state.carrierFrequency <= CARRIER_FREQUENCY_MAX_VAL
    };

    dataAreValid = () => {
        return this.snrIsValid()
            &&
            this.txGainIsValid()
            &&
            this.rxGainIsValid()
            &&
            this.carrierFrequencyIsValid()
    };

    setCurrentImageIndex(i) {
        this.setState(
            {
                selectedImageIndex: i
            }
        )
    }

    render() {
        const {classes} = this.props;
        const {activeStep} = this.state;

        return (
            <Provider value={this.getContext()}>
                <CssBaseline/>
                <main className={classes.layout}>
                    <Paper className={classes.paper}>
                        <Typography variant="display1" align="center">
                            Choose the parameters of the transmission
                        </Typography>
                        <Stepper activeStep={activeStep} className={classes.stepper}>
                            {steps.map(label => (
                                <Step key={label}>
                                    <StepLabel>{label}</StepLabel>
                                </Step>
                            ))}
                        </Stepper>
                        <React.Fragment>
                            {activeStep === steps.length ? (
                                <React.Fragment>
                                    <Typography variant="headline" gutterBottom>
                                        You have launched a new transmission!
                                    </Typography>
                                    <Typography variant="subheading">
                                        The simulation should be running already.
                                        you can keep navigating through the website
                                        and explore the results once they are ready
                                        or go directly to the results page
                                        and wait got the execution to be over.
                                    </Typography>
                                    <div>
                                        <Button variant="outlined" className={classes.button} component={
                                            props => <Link to="/" {...props}/>
                                        }>
                                            Back to home
                                        </Button>
                                        <Button variant="outlined" className={classes.button}>
                                            Results page
                                        </Button>
                                    </div>
                                </React.Fragment>
                            ) : (
                                <form>
                                    {this.getStepContent(activeStep)}
                                    <div className={classes.buttons}>
                                        {activeStep !== 0 && (
                                            <Button variant="outlined" onClick={this.handleBack} className={classes.button}>
                                                Back
                                            </Button>
                                        )}
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            onClick={this.handleNext}
                                            className={classes.button}
                                        >
                                            {activeStep === steps.length - 1 ? 'Launch!' : 'Next'}
                                        </Button>
                                    </div>
                                </form>
                            )}
                        </React.Fragment>
                    </Paper>
                </main>
            </Provider>
        );
    }
}

TransmissionSteps.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TransmissionSteps);
