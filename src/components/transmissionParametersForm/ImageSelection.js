import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ImageCarousel from '../imagecarousel/ImageCarousel';
import {withContext} from "../../context";
import PictureShooter from './PictureShooter';

const ImageSelection = props => {
    let cameraButtonText;
    let text1;

    if (props.webcamIsOpen) {
        cameraButtonText = 'Close webcam';
        text1 = ''
    }
    else {
        cameraButtonText= 'Open webcam';
        text1 = 'Or grab a new one with your camera!'
    }

    return (
        <React.Fragment>
            <Typography variant="title" gutterBottom>
                Choose the image to transmit
            </Typography>
            <Grid container spacing={24}>
                <Grid item xs={12}>
                    <ImageCarousel images={props.images} loading={props.isLoadingImages} open={props.galleryIsOpen}/>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="subheading" gutterBottom>
                        {text1}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <PictureShooter open={props.webcamIsOpen} onConfirm={props.storeImage}/>
                </Grid>
                <Grid item xs={12}>
                    <Button variant="outlined" onClick={
                        props.toggleCamera
                    }>
                        {cameraButtonText}
                    </Button>
                </Grid>
            </Grid>
        </React.Fragment>
    );
};

ImageSelection.propTypes = {};

export default withContext(ImageSelection);
