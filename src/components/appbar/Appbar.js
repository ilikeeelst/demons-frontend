import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import UserBadge from "./UserBadge";
const styles = {
    flex: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
};

const Appbar = (props) => {

    const { classes } = props;
    let slot1;
    let slot2;
    if (props.isLoggedIn) {
        slot1 =
                <UserBadge/>;
        slot2 =
                <Button color="inherit" onClick={props.openLogoutDialog}>
                    Logout
                </Button>
    }
    else {
        slot1 =
                <Button color="inherit" onClick={props.openLoginDialog}>
                    Login
                </Button>;
        slot2 =
                <Button color="inherit" onClick={props.openSignupDialog}>
                    Sign up
                </Button>;
    }
    return (
        <div className="header">
            <AppBar position="static">
                <Toolbar className="appBar">
                    <IconButton
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="Menu"
                        onClick={props.onMenuButtonClick}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="title" color="inherit" className={classes.flex}>
                    </Typography>
                    {slot1}
                    {slot2}
                </Toolbar>
            </AppBar>
        </div>
    );
};

Appbar.propTypes = {
    classes: PropTypes.object.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    openLoginDialog: PropTypes.func.isRequired,
    openSignupDialog: PropTypes.func.isRequired,
    openLogoutDialog: PropTypes.func.isRequired,
    closeLoginDialog: PropTypes.func.isRequired,
    closeSignupDialog: PropTypes.func.isRequired,
    onMenuButtonClick: PropTypes.func
};

export default withStyles(styles)(Appbar);