import React, {Fragment} from 'react'
import PropTypes from 'prop-types'
import Webcam from 'react-webcam'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';


class PictureShooter extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            confirmationDialogIsOpen: false,
            image: null,
            confirmed: false
        };
    }
    setRef = webcam => {
        this.webcam = webcam;
    };

    capture = () => {
        const imageSrc = this.webcam.getScreenshot();
        this.setState({
            confirmationDialogIsOpen: true,
            image: imageSrc
        });
    };

    closeDialog() {
        this.setState({
            confirmationDialogIsOpen: false
        })
    }

    confirmPicture = () => {
        this.props.onConfirm(this.state.image);
        this.setState({
            confirmed: true,
            confirmationDialogIsOpen: false
        });
    };

    render () {
        const videoConstraints = {
            width: 512,
            height: 512,
            facingMode: 'user',
        };

        let content;

        if (this.state.confirmed) {
            content =
                <Fragment>
                    <img src={this.state.image}/>
                    <div>
                        <Button
                            variant="contained"
                            onClick={() => {this.setState({confirmed: false})}}
                        >
                            Take another picture
                        </Button>
                    </div>
                </Fragment>

        }
        else {
            content =
                <Fragment>
                    <Webcam
                        style={{
                            margin: "10px",
                            width: "100%"
                        }}
                        audio={false}
                        height={512}
                        ref={this.setRef}
                        screenshotFormat="image/jpeg"
                        width={512}
                        videoConstraints={videoConstraints}
                    />
                    <div>
                        <Button
                            variant="contained"
                            onClick={this.capture}
                        >
                            Take a picture
                        </Button>
                    </div>
                </Fragment>
        }

        if (this.props.open) {
            return (
                <div>
                    <Dialog open={this.state.confirmationDialogIsOpen}>
                        <DialogTitle className="dialogTitle">
                            Is this picture ok?
                        </DialogTitle>
                        <DialogContent>
                            <img src={this.state.image} width="100%"/>
                            <Button className="pictureDialogButton" onClick={this.confirmPicture}>
                                Yes, use this picture
                            </Button >
                            <Button className="pictureDialogButton" onClick={this.closeDialog.bind(this)}>
                                No, take another one
                            </Button>
                        </DialogContent>
                    </Dialog>
                    {content}
                </div>
            )
        }
        else {
            return null;
        }

    }
}

PictureShooter.propTypes = {
    open: PropTypes.bool,
    onConfirm: PropTypes.func
};

export default PictureShooter