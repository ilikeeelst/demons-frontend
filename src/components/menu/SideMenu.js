import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import MenuContent from './MenuContent'
import {SPARTAN_CRIMSON} from "../../constants";
import {RICH_BLACK} from "../../constants";

const drawerWidth = 240;

const styles = () => ({
    drawerPaper: {
        width: drawerWidth,
        position: "relative",
        background: `linear-gradient(${SPARTAN_CRIMSON},${RICH_BLACK})`,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderBottomWidth: 0,
        borderRightWidth: 1,
        borderColor: "black",
        borderStyle: "dashed"
    }
});

const SideMenu = props => {
    const { classes } = props;

    return (
        <Fragment>
                <SwipeableDrawer
                    disableDiscovery
                    disableSwipeToOpen
                    onOpen={props.onOpen}
                    variant="temporary"
                    anchor='left'
                    open={props.mobileOpen}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    onClose={props.onClose}
                >
                    <MenuContent/>
                </SwipeableDrawer>
        </Fragment>
    );
};

SideMenu.propTypes = {
    classes: PropTypes.object.isRequired,
    mobileOpen: PropTypes.bool,
    onClose: PropTypes.func.isRequired,
    onOpen: PropTypes.func.isRequired
};

export default withStyles(styles)(SideMenu);