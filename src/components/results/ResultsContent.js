import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import SnrSpecificData from './SnrSpecificData';


const ResultsContent = props => {
    let snrSpecificData = [];
    for (let snr of props.methodData.snrs) {
        snrSpecificData.push(
            <SnrSpecificData
                calculatedSnr={snr.calculatedSNR}
                ssim={snr.ssim}
                sdr={snr.sdr}
                channel={snr.channel}
                outputImage={snr.outputImage}
            />
        )
    }
    return(
        <div>
            <Typography variant="title">
                Method: {props.methodName}
            </Typography>
            <Typography variant="body2">
                Method phases: {props.methodData.phases}
            </Typography>
            {snrSpecificData}
        </div>
    )
};

ResultsContent.propTypes = {
    methodName: PropTypes.string.isRequired,
    methodData : PropTypes.array.isRequired,
};

export default ResultsContent;