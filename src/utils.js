import {
    emailRegex
} from './constants';

const generateQueryString = parameters => {
  let keys = Object.keys(parameters);
  if (keys.length < 1){return "";}
  let outputString = `?${keys[0]}=${parameters[keys[0]]}`;
  for (let key of keys.slice(1)) {
      outputString += `&${key}=${parameters[key]}`
  }
  return outputString
};

export {generateQueryString};

export const stringIsEmail = string => {
    return emailRegex.test(string)
};

export const getObjectFromQueryString = () => {
    var search = window.location.search.substring(1);
    return JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
};

export const convertFileToBase64 = file => {
    let reader = new FileReader();
    let imageData = null;
    reader.onload = (e) => {
        imageData = e.target.result;
        return btoa(imageData)
    };
    return reader.readAsBinaryString(file);
};

export const getDatetimeString = isoFormattedDatetime => {
    let date = new Date(isoFormattedDatetime);
    let year = date.getFullYear();
    let month = date.getMonth();
    let day = date.getDay();
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let today = new Date();
    let currentYear = today.getFullYear();
    let currentMonth = today.getMonth();
    let currentDay = today.getDay();

    if (year === currentYear && month === currentMonth && day === currentDay) {
        return hours + ":" + minutes;
    }
    else {
        return day + "/"+ (month+1) + "/" + year;
    }
};