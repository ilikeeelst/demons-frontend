import React from 'react'
import Grid from "@material-ui/core/Grid/Grid";
import Typography from "@material-ui/core/Typography/Typography";
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import {RICH_BLACK} from "../../constants";

const styles = theme => ({
    footer: {
        marginTop: theme.spacing.unit * 8,
        borderTop: `1px solid ${theme.palette.divider}`,
        padding: `${theme.spacing.unit * 6}px 0`,
        background: RICH_BLACK
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(900 + theme.spacing.unit * 3 * 2)]: {
            width: 900,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
});

const footers = [
    {
        title: 'Want to learn more?',
        items: [
            {
                name: 'JSCC',
                url: 'https://en.wikipedia.org/wiki/Joint_source_and_channel_coding'
            },
            {
                name: "The FAWIN project",
                url: "http://gtec.des.udc.es/web/index.php?option=com_content&view=article&id=149&Itemid=83"
            }
            ],
    },
    {
        title: 'External Resources',
        items: [
            {
                name: "GTEC",
                url: "http://gtec.des.udc.es/web/"
            },
            {
                name: "UDC",
                url: "https://www.udc.es/"
            }
            ]
    },
    {
        title: 'Admins zone',
        items: [{
            name: "Image Manager",
            url: "http://localhost:3000/admin"
        }],
    },
    {
        title: 'About',
        items: [
            {
                name: "© 2018 Demons",
                url: "http://localhost:3000"
            },
            {
                name: "Adriano Todaro",
                url: "mailto:adriano.todaro@udc.es"
            }
        ],
    },
];

const Footer = props => {
    const { classes } = props;
    return (
        <footer className={classNames(classes.footer, classes.layout)}>
            <Grid container spacing={32} justify="space-evenly">
                {footers.map(footer => (
                    <Grid item xs key={footer.title}>
                        <Typography variant="title" color="textPrimary" gutterBottom>
                            {footer.title}
                        </Typography>
                        {footer.items.map(item => (
                            <Typography key={item} variant="body2" color="textSecondary">
                                <a
                                    href={item.url}
                                    target="_blank"
                                    className="link"
                                    rel="noopener noreferrer"
                                    >
                                    {item.name}
                                </a>
                            </Typography>
                        ))}
                    </Grid>
                ))}
            </Grid>
        </footer>
    )

};

export default withStyles(styles)(Footer);


/*

        <div className="footer">
            <Toolbar>
                © 2018 Demons
                <a
                    style={{ padding: '0px 20px' }}
                    href="http://gtec.des.udc.es/web/"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    GTEC
                </a>
                <p>
                    Created By <b/>
                <a href="mailto:adriano.todaro@udc.es" target="_top">
                    <strong>Adriano Todaro</strong>
                </a>
                </p>
                <a
                    style={{ padding: '0px 20px' }}
                    href="https://www.udc.es/"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Universidade da Coruña
                </a>
            </Toolbar>
        </div>
 */